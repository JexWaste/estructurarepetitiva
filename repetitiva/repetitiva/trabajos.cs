﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace repetitiva
{
    class trabajos
    {
        public static void suma()
        {
            ///Sumar los números enteros de 1 a 100 mediante: a) estructura repetir; b) estructura mientras; c) estructura desde
            Console.WriteLine("Sumar los números enteros de 1 a 100 mediante:");
            Console.WriteLine("a) estructura repetir");
            Console.WriteLine("b) estructura mientras");
            Console.WriteLine("c) estructura desde");
            string a;
            a = Console.ReadLine();
            switch(a)
            {
                case "a":
                    Console.WriteLine("Estructura Repetir:");
                    int i;
                    int suma = 0;
                    for(i=0;i<101; i++)
                    {
                        suma = suma + i;
                        Console.WriteLine(suma);
                    }
                    break;
                case "b":
                    Console.WriteLine("Estructura Desde");
                    int e = 0;
                    int suma1 = 0;
                    while (e < 100)
                    {
                        e++;
                        suma1 = suma1 + e;
                        Console.WriteLine(suma1);
                    }
                    break;
                case "c":
                    Console.WriteLine("Estructura Desde");
                    int o = 0;
                    int suma2 = 0;
                    do
                    {
                        o++;
                        suma2 = suma2 + o;
                        Console.WriteLine(suma2);
                    } while (o < 100);
                    break;

            }

        }
        public static void calif()
        {
            ///Se desea leer las calificaciones de una clase de informática y contar el número total de aprobados (5 o mayor que 5).
            int alumnos;
            double calif;
            int aprob=0;
            int reprob=0;
            int i;
            Console.WriteLine("Ingresa el numero de alumnos");
            alumnos = Convert.ToInt32(Console.ReadLine());
            for (i = 1; i < alumnos + 1; i++)
            {
                Console.WriteLine("Inserta la calificacion del alumno #" + i);
                calif = Convert.ToDouble(Console.ReadLine());
                if(calif==5||calif>5)
                {
                    aprob++;
                }
                else
                {
                    reprob++;
                }
            }
            Console.WriteLine("La cantidad de alumnos aprobados es: " + aprob);
            Console.WriteLine("La cantidad de alumnos reprobados es: " + reprob);

        }
    }
}
